Serveur local

Installer JSON-SERVER : `npm i -g json-server`

Faire tourner le back : `json-server --w src/assets/db.json --port 3003`


Chartjs

dataLabel : `npm install chartjs-plugin-datalabels --save`

chartjs-plugin-style : `npm install chartjs-plugin-style --save`