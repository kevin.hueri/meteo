import React, { useEffect } from "react";
import MeteoDay from "./components/MeteoDay";
import NavBar from "./components/NavBar";
import MeteoWeek from "./components/MeteoWeek"
import { useSelector } from "react-redux";

import { store } from "./store";

import { getWeather } from "./store/actions/WeatherActions";

const App = () => {
  const data = useSelector((state) => state.weather.data);

  useEffect(() => {
    store.dispatch(getWeather());
  }, [data]);

  return (
    <div>
      {data.weather && (
        <div id={data.weather[0].main}>
          <MeteoDay />
          <MeteoWeek />
          <NavBar />
        </div>
      )}
    </div>
  );
};

export default App;
