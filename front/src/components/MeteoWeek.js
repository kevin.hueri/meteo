import { CardContent } from "@mui/material";
import Container from "@mui/material/Container";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { store } from "../store";
import { getWeathersWeek } from "../store/actions/WeatherActions";
import CardMeteoWeek from "./CardMeteoWeek";

const WeatherWeek = () => {
  const data = useSelector((state) => state.weather.data);
  const dataWeek = useSelector((state) => state.weather.dataWeek);

  useEffect(() => {
    if (data.weather) {
      store.dispatch(getWeathersWeek(data.coord.lat, data.coord.lon));
    }
  }, [data]);

  return (
    <React.Fragment>
      <Container className="meteoWeek" maxWidth="xl">
        <p className='titleMeteoWeek'>Météo pour la semaine</p>
        {data.weather && dataWeek.daily && (
          <CardContent className="meteoWeekUl">
            {dataWeek.daily.map((data, index) => (
              <CardMeteoWeek daily={data} key={index} />
            ))}
          </CardContent>
        )}
      </Container>
    </React.Fragment>
  );
};

export default WeatherWeek;
