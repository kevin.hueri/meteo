import React, { useEffect } from "react";
import { Line } from "react-chartjs-2";
import moment from "moment";
import { useSelector } from "react-redux";
import { store } from "../store";
import { getWeathersWeek } from "../store/actions/WeatherActions";
import Container from "@mui/material/Container";
import { Chart } from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";


const LineChart = () => {
  // Register the plugin to all charts:
  Chart.register(ChartDataLabels);


  const data = useSelector((state) => state.weather.data);
  const dataWeek = useSelector((state) => state.weather.dataWeek);
  const startDate = new Date(Date.now());
  const tomorrow = startDate.setDate(startDate.getDate() + 1);
  const labels = [];
  const tempDay = [];
  for (let i = 0; i < 7; i++) {
    const date = moment(tomorrow).add(i, "days").format("DD/MM");
    labels.push(date.toString());
  }

  if (dataWeek.daily)
    dataWeek.daily.map((item) => {
      tempDay.push(Math.round(item.temp.day));
    });

  const dataGraph = {
    labels: labels,
    datasets: [
      {
        label: "Température min",
        data: tempDay,
        borderWidth: 3,
        borderColor: "white",
        tension: 0.4,
        datalabels: {
          align: 'start',
          color: "white",
          font: {
            weight: "bold",
          },
          padding: 6,
        },
      },
    ],
  };

  const options = {
    maintainAspectRatio: false,
    scales: {
      x: {
        grid: {
          display: false
        },
        ticks: {
          display: true
        }
      },
      y: {
        display: false,
      }
    },
    plugins: {
      legend: {
        display: false,
      },
    }
  }


  useEffect(() => {
    if (data.weather) {
      store.dispatch(getWeathersWeek(data.coord.lat, data.coord.lon));
    }
  }, [data]);


  return (
    <React.Fragment>
      <Container className="containerGraph" maxWidth="xl">
        <div className="GraphTemp">
          <Line id="lineTemp" data={dataGraph} options={options} />
        </div>
      </Container>
    </React.Fragment>
  )
}

export default LineChart;