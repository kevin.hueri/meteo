import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CityName from "./MeteoDayComponents/CityName";
import Temperature from "./MeteoDayComponents/Temperature";
import WeatherDescription from "./MeteoDayComponents/WeatherDescription";
import Humidity from "./MeteoDayComponents/Humidity";
import Pressure from "./MeteoDayComponents/Pressure";
import Wind from "./MeteoDayComponents/Wind";
import FavoriteIcon from "./MeteoDayComponents/FavoriteIcon";
import Visibility from "./MeteoDayComponents/Visibility";
import FeelLike from "./MeteoDayComponents/FeelLike";
import Clouds from "./MeteoDayComponents/Clouds";

import { store } from "../store";

import { getCountry } from "../store/actions/FavoriteActions";

const MeteoDay = () => {
  const data = useSelector((state) => state.weather.data);

  useEffect(() => {
    store.dispatch(getCountry());
  }, [data]);


  // console.log(Math.floor(Date.now() / 1000));
  // const [weatherStyle, setWeatherStyle] = useState("night");
  // const [playOnce, setPlayOnce] = useState(true);

  // if(playOnce){
  //   if (data.sys) {
  //     const sunRise = data.sys.sunrise;
  //     const sunSet = data.sys.sunset;
  //     const time = Math.floor(Date.now() / 1000);
  // console.log(sunRise);
  // console.log(sunSet);
  // console.log(time);
  //     if (sunRise < time && time < sunSet) {
  //       setWeatherStyle(data.weather[0].main);
  //       setPlayOnce (false);
  //     } else {
  //       setWeatherStyle("night");
  //       setPlayOnce (false);
  //     }
  //   }
  // }
  // console.log(weatherStyle);

  return (
    <Card className="meteoDay">
      {data.weather && (
        <Card className="CountryIndice">

          <Card className="cardText" >

            <CardContent>

              <CityName />
              <Temperature />
              <WeatherDescription />

              <CardContent className="dayContainer">
 
                  <FeelLike />
                  <Clouds />
                  <Humidity />
                  <Pressure />
                  <Wind />
                  <Visibility />
                
              </CardContent>

            </CardContent>

          </Card>

          <Card className="cardIcon" sx={{ maxWidth: 345 }}>
            <FavoriteIcon />
          </Card>

        </Card>
      )
      }
    </Card >
  );
};

export default MeteoDay;
