import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Temperature = () => {
  const data = useSelector((state) => state.weather.data);
  return (
    <div>
      <Typography
        gutterBottom
        className="temperature"
        variant="h5"
        component="div"
      >
        {Math.round(data.main.temp)}°
      </Typography>
    </div>
  );
};

export default Temperature;
