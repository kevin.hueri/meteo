import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Visibility = () => {
    const data = useSelector((state) => state.weather.data);

    return (
        <Typography className="meteoDayCard">
            Visibilité: {Math.round((data.main.pressure) / 100)} km
        </Typography>
    )
}

export default Visibility
