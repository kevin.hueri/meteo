import React, { useState } from "react";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import { useSelector } from "react-redux";
import NotFavorite from "../../assets/addFavoris.svg";
import Favorite from "../../assets/deleteFavoris.svg";

// import { store } from "../../store";

// import { postCountry } from "../../store/actions/FavoriteActions";
// import { deleteCountry } from "../../store/actions/FavoriteActions";

const FavoriteIcon = () => {
  const [favoris, setFavoris] = useState("");


  const data = useSelector((state) => state.weather.data);
  const dataCountry = useSelector((state) => state.favorite.dataCountry);
  // console.log('datacountry', dataCountry[0].ville)

  function handleChangeFavorite(e) {
    const checkExist = dataCountry.some((item) => item.ville === data.name);

    // console.log("checklist", checkExist)
    if(checkExist) {
      setFavoris = Favorite;
    } else {
      setFavoris = NotFavorite;
    } 
  }

  return (
    <CardActions>
      <Button
        size="small"
        variant="contained"
        id="favoriteButton"
        value={data.name}
        onClick={(e) => handleChangeFavorite(e)}
      >
        <img src={favoris} alt="Favoris" />
      </Button>
    </CardActions>
  );
};

export default FavoriteIcon;
