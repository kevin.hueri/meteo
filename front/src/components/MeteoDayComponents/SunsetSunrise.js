import React from "react";
import Sunrise from "../../assets/sunrise.svg";
import Sunset from "../../assets/sunset.svg";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import Card from "@mui/material/Card";
import { useSelector } from "react-redux";

const SunsetSunrise = () => {
  const data = useSelector((state) => state.weather.data);

  return (
    <div>
      
      <Card className="sunriseSunset">

        {/* Levée de soleil */}
        <CardContent className="sunriseCard">
        <CardMedia
          component="img"
          height="35"
          src={Sunrise}
          className="imageSun"
          alt="Couché de soleil"
        />
        <Typography
          className="weatherDescrSun"
          variant="body2"
          color="text.secondary"
        >
          {new Date(data.sys.sunset * 1000).toLocaleString().slice(12, 17)}
        </Typography>
      </CardContent>

        {/* Couchée de soleil */}
        <CardContent className="sunsetCard">
          <CardMedia
            component="img"
            height="35"
            src={Sunset}
            className="imageSun"
            alt="Couché de soleil"
          />
          <Typography
            className="weatherDescrSun"
            variant="body2"
            color="text.secondary"
          >
            {new Date(data.sys.sunset * 1000).toLocaleString().slice(12, 17)}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
};
export default SunsetSunrise;
