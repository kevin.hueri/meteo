import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const CityName = () => {
  const data = useSelector((state) => state.weather.data);
  return (
    <div>
      <Typography gutterBottom className="ville" variant="h5" component="div">
        {data.name}
      </Typography>
    </div>
  );
};

export default CityName;
