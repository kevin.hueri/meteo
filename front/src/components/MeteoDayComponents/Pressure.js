import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Pressure = () => {
  const data = useSelector((state) => state.weather.data);

  return (
    <Typography className="meteoDayCard">
      Pression: {data.main.pressure}hPa
    </Typography>
  );
};

export default Pressure;
