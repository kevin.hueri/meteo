import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Humidity = () => {
  const data = useSelector((state) => state.weather.data);

  return (
    <Typography className="meteoDayCard">
      Humidité: {data.main.humidity}%
    </Typography>
  );
};

export default Humidity;
