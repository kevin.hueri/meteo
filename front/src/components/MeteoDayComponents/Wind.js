import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Wind = () => {
  const data = useSelector((state) => state.weather.data);

  return (
    <Typography className="meteoDayCard">
      Vent: {Math.round(data.wind.speed * 3.6)} km/h
    </Typography>
  );
};

export default Wind;
