import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const FeelLike = () => {
  const data = useSelector((state) => state.weather.data);
  
  return (
    <Typography gutterBottom className="meteoDayCard">
      Température ressentie: {Math.round(data.main.feels_like)}°
    </Typography>
  );
};

export default FeelLike;
