import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const Clouds = () => {
  const data = useSelector((state) => state.weather.data);
  
  return (
    <Typography gutterBottom className="meteoDayCard">
      Couverture nuageuse: {data.clouds.all}%
    </Typography>
  );
};

export default Clouds;
