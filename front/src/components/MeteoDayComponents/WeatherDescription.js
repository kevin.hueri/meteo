import React from "react";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";

const WeatherDescription = () => {
  const data = useSelector((state) => state.weather.data);

  return (
    <Typography className="weatherDescription">
      {data.weather[0].description}
    </Typography>
  );
};

export default WeatherDescription;
