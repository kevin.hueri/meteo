import * as React from "react";
import Card from "@mui/material/Card";
import Typography from '@mui/material/Typography';
import dateFormatMeteo from "../utils/DateFormatMeteo";
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

export default function CardMeteoWeek(data) {

  return (
    <Card className="CardMeteoWeek" sx={{ maxWidth: 345 }}>

      {/* date */}
      <Typography className="meteoWeekDescription" gutterBottom variant="h5" component="div">
        {dateFormatMeteo(data.daily.dt)}
      </Typography>

      {/* icone */}
      <CardMedia
        component= "li"
        className={data.daily.weather[0].main}
        height="140"
      />

      <CardContent>
        {/* Température min */}
        <Typography className="meteoWeekDescription tempWeek">
          min: {Math.round(data.daily.temp.min)}°
        </Typography>

        {/* Température max */}
        <Typography className="meteoWeekDescription tempWeek">
          max: {Math.round(data.daily.temp.max)}°
        </Typography>
      </CardContent>

      {/* Description temps */}
      <Typography className="meteoWeekDescription">
        {data.daily.weather[0].description}
      </Typography>

    </Card>
  );
};

