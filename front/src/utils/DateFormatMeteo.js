  // Format Date
  const dateFormatMeteo = (date) => {
    const dateUnix = new Date(date * 1000);
    let newDate = new Date(dateUnix).toLocaleDateString("fr-FR", {
      weekday: "short",
    });
    return newDate;
  };

  export default dateFormatMeteo;