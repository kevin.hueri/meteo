/* 
 * Action types {...}
 * ****************** */

// Weather
export const GET_WEATHER_DATA = "GET_WEATHER_DATA";

// WeatherWeek
export const GET_WEATHERSWEEK_DATA = "GET_WEATHERSWEEK_DATA";

// Country
export const GET_COUNTRY_DATA = "GET_COUNTRY_DATA";
export const ADD_COUNTRY_DATA = "ADD_COUNTRY_DATA";
export const DELETE_COUNTRY_DATA = "DELETE_COUNTRY_DATA";