/*
 * Import - Module
 * *************** */
import axios from "axios";

/*
 * Import types {...}
 * ****************** */
import {
  GET_COUNTRY_DATA,
  ADD_COUNTRY_DATA,
  DELETE_COUNTRY_DATA,
} from "./ActionTypes.js";

/*
 * Actions
 * ******* */
  
  // Get Country
  export const getCountry = () => {
    return (dispatch) => {
      return axios
        .get("http://localhost:3003/api/favorite")
        .then((res) => {
          // console.log('res favorite', res.data)
          dispatch({ 
            type: GET_COUNTRY_DATA, 
            payload: res.data.dbFavorite 
          });
        })
        .catch((err) => console.log(err));
    };
  };
  
  // Post Country
  export const postCountry = (data) => {
    return (dispatch) => {
      return (
        axios
          .post("http://localhost:3003/api/favorite", data)
          .then((res) => {
            console.log("postFavorite", res.data.dbFavorite);
            dispatch({
              type: ADD_COUNTRY_DATA,
              payload: res.data.dbFavorite
            });
          })    
          .catch((err) => console.log(err))
      );
    };
  };

  // Delete Country
export const deleteCountry = (id) => {
  return (dispatch) => {
    return axios
      .delete("http://localhost:3003/api/favorite/" + id)
      .then((res) => {
        console.log('deleteCountry', res.data.dbFavorite);
        dispatch({
          type: DELETE_COUNTRY_DATA,
          payload: res.data.dbFavorite
        });
      })
      .catch((err) => console.log(err));
  };
}

  