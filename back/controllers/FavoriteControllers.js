/*
 * Favorite Controllers
 * ****************** */

// Import Model
const Favorite = require("../models/FavoriteModel");



// GetAll
exports.getAll = async (req, res) => {

    // Recupération des données dans la DB
    const dbFavorite = await Favorite.find();

    // Résultat dans le json
    res.json({
        message: "Voici vos favoris",
        dbFavorite
    });
};



// GetOne
exports.getId = async (req, res) => {

    // Recupération des données selectoinnées dans la DB
    const favoriteID = await Favorite.findById(req.params.id);

    // Résultat dans le json
    res.json({
        message: "Voici votre Favoris",
        favoriteID
    });

}



// Create
exports.create = async (req, res) => {
    console.log('create favorite back: ', req.body)

    // Variable pour diminuer la suite des requetes
    const b = req.body;

    // Condition pour vérifier si nous avons un name et un message dans le body
    if (b.ville) {

        // On définit la construction de notre article
        const favoriteTown = new Favorite({
            ville: b.ville,
        });

        // Et on sauvegarde nos modifications
        favoriteTown.save((err) => {});

        // Recupération des données dans la DB
        const dbFavorite = await Favorite.find();

        // Resultat affiché sur Postman lors de la création
        res.json({
            message: "Item crée avec succes !",
            dbFavorite
        });

        // Resultat affiché sur Postman lors de l'aboutissement de la requete
    } else res.json({
        message: "Erreur, l'item n'as pas été créé !"
    });
};



// EditOne
exports.editOne = async (req, res) => {
    console.log('editOne', req.params)

    // Récupération des données séléctionnées et modification de celles ci
    await Favorite.findByIdAndUpdate(req.params.id, {
        ...req.body
    })

    // Résultat renvoyé
    res.json({
        message: "Item edité avec succes !"
    });
};



// DeleteAll
exports.deleteAll = async (req, res) => {

    // Recupération des données dans la DB
    const dbFavorite = await Favorite.find();

    // Suppression de la totalité de la table News
    await Favorite.deleteMany();

    // Resultat affiché sur Postman lors de l'aboutissement de la requete
    res.json({
        message: "Tous les items ont été supprimés avec succes !"
    });
};



// DeleteOne
exports.deleteOne = async (req, res) => {

    console.log('DeleteOne News', res.params)

    // Recupération des données sélectionnées dans la DB
    const FavoriteId = await Favorite.findById(req.params.id);

    // Suppression de l'id de la DB
    await Favorite.findByIdAndDelete(FavoriteId);

    // Recupération des données dans la DB
    const dbFavorite = await Favorite.find();

    // Resultat affiché sur Postman lors de l'aboutissement de la requete
    res.json({
        message: "L'item a été supprimé avec succes !",
        dbFavorite
    });
}
