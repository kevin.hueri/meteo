/*
 * Import Module
 * ************* */ 

const express = require('express')
const router = express.Router()

// Controllers
const FavoriteControllers = require('./controllers/FavoriteControllers')

// MIddleware

/*
 * Routes
 * ****** */ 

// '/'

// Route News '/'
router
  .get("/favorite", FavoriteControllers.getAll)
  .get("/favorite/:id", FavoriteControllers.getId)
  .post("/favorite", FavoriteControllers.create)
  .put("/favorite/:id", FavoriteControllers.editOne)
  .delete("/favorite", FavoriteControllers.deleteAll)
  .delete("/favorite/:id", FavoriteControllers.deleteOne)

module.exports = router
