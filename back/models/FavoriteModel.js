/*
 *
 * Model de 'Favorite'
 ******************************/

// Import de Mongoose
const mongoose = require('mongoose')
const Schema = mongoose.Schema


// Création de notre Shéma (Model)
// c'est le Model de (Model)
const FavoriteSchema = new Schema({
    ville: {
        type: String,
    }
})

// Et l'on export notre model grace à la passerelle Mongoose
// Ce qui nous permettra de pouvoir l'utiliser sur d'autre page
module.exports = mongoose.model('Favorite', FavoriteSchema)
